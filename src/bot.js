import Telegraf from 'telegraf'
import Markup from 'telegraf/markup'
import axois from 'axios'
import express from 'express'

require('dotenv').config()

const app = express()

const bot = new Telegraf(process.env.BOT_TOKEN, { telegram: { webhookReply: false } })

app.use(bot.webhookCallback('/'))

const clientId = process.env.CLIENT_ID
const apiUrl = 'https://api.soundcloud.com/'

bot.start((ctx) => {
  if (ctx.chat.type === 'private') {
    return ctx.reply('\uD83C\uDFB5 Я помогу тебе найти музыку в SoundCloud\n\n➖ Для того чтобы найти музыку, просто введи её название и отправь мне.')
  }
})

bot.on('text', async (ctx) => {
  const msg = await ctx.reply('loading', {
    reply_to_message_id: ctx.message.message_id
  })
  const tracks = (await axois.get(`${apiUrl}tracks`, {
    params: {
      q: ctx.message.text, limit: 10, client_id: clientId
    }
  })).data
  if (tracks.length === 0) {
    await bot.telegram.editMessageText(msg.chat.id, msg.message_id, null, 'Ничего не найдено')
    return
  }
  const tracksMarkup = []
  tracks.map((track) => {
    tracksMarkup.push([Markup.callbackButton(`${track.user.username} - ${track.title}`, `stream_${track.id}`)])
    return null
  })
  await bot.telegram.editMessageText(msg.chat.id, msg.message_id, null, ctx.message.text, {
    reply_markup: Markup.inlineKeyboard(tracksMarkup)
  })
})

bot.action(/stream_[0-9]+/, async (ctx) => {
  await ctx.answerCbQuery('Loading')
  const trackId = ctx.callbackQuery.data.match(/stream_([0-9]+)/)[1]
  const track = (await axois.get(`${apiUrl}tracks/${trackId}`, {
    params: {
      client_id: clientId
    }
  })).data
  return ctx.replyWithAudio({ url: `${track.stream_url}?client_id=${clientId}`, filename: 'sound.mp3' }, {
    caption: `<a href="${track.permalink_url}">${track.user.username} - ${track.title}</a>`,
    title: track.title,
    performer: track.user.username,
    parse_mode: 'html'
  })
})

bot.telegram.setWebhook(process.env.WEBHOOK_URL).then(() => {
  app.listen(3000, () => console.log('bot started!'))
})
